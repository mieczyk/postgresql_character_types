require 'io/console'
require 'benchmark'
require 'fileutils'
require_relative '../lib/data_generator'
require_relative '../lib/database_manager'
require_relative '../lib/display_helper_methods'

BASEDIR = "#{__dir__}/.."

DB_NAME = 'performance_test'
TABLE_NAME = 'strings_test'
STRING_LENGTHS = [10, 50, 100]
STRINGS_COUNT = 500_000
TRIALS = 5
SELECTS_COUNT = 20

if ARGV.length < 1
  puts "Usage: #{__FILE__} <PG_USER>"
  exit 1
end

pg_user = ARGV[0]

print "Password for user #{pg_user}: "
pg_password = STDIN.noecho(&:gets).chop

puts '[*] Connecting to the PostgreSQL server...'
db = DatabaseManager.new pg_user, pg_password

puts "[*] Remove database '#{DB_NAME}' if exists..."
db.drop_database_if_exists DB_NAME

puts "[*] Create database '#{DB_NAME}'..."
db.create_database DB_NAME

puts '[*] Create test data files:'
FileUtils.mkdir_p "#{BASEDIR}/data"
STRING_LENGTHS.each do |length|
  file_name = "#{BASEDIR}/data/strings_#{length}.txt"

  puts "- #{file_name}"

  File.open(file_name, 'w') do |f|
    f.puts DataGenerator::generate_strings(length, STRINGS_COUNT)
  end
end

# Reconnect to the newly created database
db = DatabaseManager.new pg_user, pg_password, DB_NAME

puts '[*] Testing of: create table, load data and create index...'
STRING_LENGTHS.each do |length|
  puts "- Testing string length: #{length}"
  ["char(#{length})", "varchar(#{length})", 'varchar', 'text'].each do |data_type|
    times = []

    1.upto(TRIALS) do
      db.drop_table_if_exists TABLE_NAME

      time_elapsed = Benchmark.realtime do
        db.create_table TABLE_NAME, "string #{data_type}"
        db.copy_from_file TABLE_NAME, File.absolute_path("#{BASEDIR}/data/strings_#{length}.txt")
        db.create_index 'idx_string', TABLE_NAME, 'string'
      end

      times.push time_elapsed
    end

    printf "\t- %-20s ", "#{data_type}:"
    print_benchmark_report times
  end
end

puts '[*] Testing of: create table with index and then load data...'
STRING_LENGTHS.each do |length|
  puts "- Testing string length: #{length}"
  ["char(#{length})", "varchar(#{length})", 'varchar', 'text'].each do |data_type|
    times = []

    1.upto(TRIALS) do
      db.drop_table_if_exists TABLE_NAME

      time_elapsed = Benchmark.realtime do
        db.create_table TABLE_NAME, "string #{data_type}"
        db.create_index 'idx_string', TABLE_NAME, 'string'
        db.copy_from_file TABLE_NAME, File.absolute_path("#{BASEDIR}/data/strings_#{length}.txt")
      end

      times.push time_elapsed
    end

    printf "\t- %-20s ", "#{data_type}:"
    print_benchmark_report times
  end
end

puts '[*] Testing of: searching indexed big tables...'
length = STRING_LENGTHS.last
file_name = File.absolute_path("#{BASEDIR}/data/strings_#{length}.txt")

puts "- Testing string length: #{length}, selects count: #{SELECTS_COUNT}"
["char(#{length})", "varchar(#{length})", 'varchar', 'text'].each do |data_type|
  db.drop_table_if_exists TABLE_NAME
  db.create_table TABLE_NAME, "string #{data_type}"
  db.copy_from_file TABLE_NAME, file_name
  db.create_index 'idx_string', TABLE_NAME, 'string'

  times = []

  1.upto(TRIALS) do
    time_elapsed = Benchmark.realtime do
      1.upto(SELECTS_COUNT) do
        where = "string = '#{File.readlines(file_name).sample}'"
        db.select_from_table TABLE_NAME, '*', where
      end
    end

    times.push time_elapsed
  end

  printf "\t- %-20s ", "#{data_type}:"
  print_benchmark_report times
end
