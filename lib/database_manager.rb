require 'pg'

class DatabaseManager
  def initialize(user, password, db_name = 'postgres')
    @connection = PG.connect dbname: db_name, user: user, password: password
  end

  def create_database(name)
    @connection.exec "CREATE DATABASE #{name}"
  end

  def drop_database_if_exists(name)
    result = @connection.exec "SELECT COUNT(1) AS db_exists FROM pg_database WHERE datname='#{name}'"
    if result[0]['db_exists'] == '1'
      @connection.exec "DROP DATABASE #{name}"
    end
  end

  def create_table(name, *columns)
    columns_string = columns.join ','
    @connection.exec "CREATE TABLE #{name} (#{columns_string})"
  end

  def drop_table_if_exists(name)
    @connection.exec "DROP TABLE IF EXISTS #{name} CASCADE"
  end

  def copy_from_file(table_name, file_name)
    @connection.exec "COPY #{table_name} FROM '#{file_name}'"
  end

  def create_index(name, table_name, field_name)
    @connection.exec "CREATE INDEX #{name} ON #{table_name} (#{field_name})"
  end

  def select_from_table(name, columns, where)
    @connection.exec "SELECT #{columns} FROM #{name} WHERE #{where}"
  end
end
