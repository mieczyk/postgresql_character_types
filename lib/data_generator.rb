class DataGenerator
  ALLOWED_STRING_CHARACTERS = [' ', *'0'..'9', *'A'..'Z', *'a'..'z']

  def self.generate_strings(length, count)
    strings = []
    1.upto(count) do
      string = (1..length).map {ALLOWED_STRING_CHARACTERS.sample }.join
      strings.push string
    end
    strings
  end
end
