def print_benchmark_report(times)
  trials = times.length
  avg = times.inject(:+) / trials

  printf_args = [trials, avg, times.min, times.max]

  printf "Trials: %i\tAverage: %.2fs\tMin: %.2fs\tMax: %.2fs\n", *printf_args
end
